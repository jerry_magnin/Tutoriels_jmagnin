Ce repo regroupe les tutoriels que je publie occasionellement sur [mon site perso](http://www.jmagnin.fr) :
* soit sur le blog, rubriques [l’astuce de la semaine](http://www.jmagnin.fr/?cat=8) et [le code de la semaine](http://www.jmagnin.fr/?cat=9), pour des astuces ou des tutos courts, s’attachant généralement à expliciter un point particulier ;
* soit sur [la page des tutoriels](http://www.jmagnin.fr/?page_id=28) pour ceux qui sont plus volumineux.

Ils concernent évidemment les logiciels libres et le développement.

Ces tutoriels sont aussi ceux de la communauté :
* par leur licence [CC-By-SA](http://creativecommons.org/licenses/by-sa/4.0/) ;
* par leurs sujets : vous pouvez me poser une question, ou me proposer un thème à traiter, tant que ceci concerne les logiciels libres ou les langages :
	* C++ ;
	* Python ;
	* LibreOffice Basic ;
	* Bash.

Pour me proposer un sujet, vous pouvez ouvrir un commentaire sur mon site, ou [une issue ici](https://git.framasoft.org/Rupicapra-rupicapra/Tutoriels_jmagnin/issues/new?issue[assignee_id]=&issue[milestone_id]=), en lui assignant le label « suggestion ».
